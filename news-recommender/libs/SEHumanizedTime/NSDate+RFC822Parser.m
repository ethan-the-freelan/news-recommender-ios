//
//  NSDate+RFC822Parser.m
//  news-recommender
//
//  Created by Ethan Nguyen on 3/24/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "NSDate+RFC822Parser.h"

@implementation NSDate (RFC822Parser)

+ (NSDate*)rfc822DateFromString:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    dateFormatter.dateFormat = @"EEE, dd MMM yyyy HH:mm:ss z";
    return [dateFormatter dateFromString:dateString];
}

@end
