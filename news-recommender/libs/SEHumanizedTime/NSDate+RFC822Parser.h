//
//  NSDate+RFC822Parser.h
//  news-recommender
//
//  Created by Ethan Nguyen on 3/24/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (RFC822Parser)

+ (NSDate*)rfc822DateFromString:(NSString *)dateString;

@end
