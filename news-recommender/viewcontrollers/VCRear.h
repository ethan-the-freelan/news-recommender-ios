//
//  VCRear.h
//  news-recommender
//
//  Created by Ethan Nguyen on 1/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCBaseNavigation.h"

@interface VCRear : VCBaseNavigation <UITableViewDelegate, UITableViewDataSource> {
    NSArray *rearNames;
    IBOutlet UITableView *tblRear;
}

@end
