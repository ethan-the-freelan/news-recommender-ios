//
//  VCBrowseScreen.m
//  news-recommender
//
//  Created by Ethan Nguyen on 3/24/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCBrowseScreen.h"
#import "VCArticlesList.h"
#import "VBrowseCell.h"

@interface VCBrowseScreen ()

- (void)gotoMenu;

@end

@implementation VCBrowseScreen

- (id)initWithListings:(NSArray *)listings andTitle:(NSString *)title {
    if (self = [super init]) {
        listingsData = listings;
        self.title = title;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customTitleWithText:self.title];
    [self customBarButtonWithImage:@"btn-navbar-menu" title:nil target:self action:@selector(gotoMenu) position:-1];
}

- (void)gotoMenu {
    [self.navigationController.viewDeckController toggleLeftView];
}

#pragma UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VCArticlesList *vcArticleList = [[VCArticlesList alloc] initWithListing:listingsData[indexPath.row]];
    [self.navigationController pushViewController:vcArticleList animated:YES];
}

#pragma UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [listingsData count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VBrowseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BrowseCell"];
    
    if (!cell)
        cell = [[VBrowseCell alloc] init];
    
    [cell updateCellWithData:listingsData[indexPath.row]];
    
    return cell;
}

@end
