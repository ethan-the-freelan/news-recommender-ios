//
//  VCArticleDetail.h
//  news-recommender
//
//  Created by Ethan Nguyen on 1/11/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCBaseNavigation.h"
#import "VArticleCell.h"

@interface VCArticlesList : VCBaseNavigation <UITableViewDelegate, UITableViewDataSource, ArticleDelegate> {
    NRBase *listingData;
    NSMutableArray *articlesData;
    
    IBOutlet UITableView *tblArticles;
    
    VArticleCell *tmpArticleCell;
}

- (id)initWithListing:(NRBase*)listing;

@end
