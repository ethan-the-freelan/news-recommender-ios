//
//  VCRear.m
//  news-recommender
//
//  Created by Ethan Nguyen on 1/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCRear.h"
#import "VCBrowseScreen.h"
#import "VCArticlesList.h"
#import "VRearCell.h"

@interface VCRear ()

@end

@implementation VCRear

- (void)viewDidLoad {
    [super viewDidLoad];
    
    rearNames = @[@"HOME", @"ALL SITES", @"ALL CATEGORIES"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.viewDeckController
     closeLeftViewAnimated:YES
     completion:^(IIViewDeckController *controller) {
         UINavigationController *navigation = nil;
         VCBaseNavigation *centerVC = nil;
         
         switch (indexPath.row) {
             case 0:
                 centerVC = [[VCArticlesList alloc] initWithListing:nil];
                 break;
                 
             case 1:
                 centerVC = [[VCBrowseScreen alloc] initWithListings:[[NRVertical currentVertical] sites]
                                                            andTitle:rearNames[1]];
                 break;
                 
             case 2:
                 centerVC = [[VCBrowseScreen alloc] initWithListings:[[NRVertical currentVertical] categories]
                                                            andTitle:rearNames[2]];
                 break;
                 
             default:
                 break;
         }
         
         if (centerVC) {
             navigation = [[UINavigationController alloc] initWithRootViewController:centerVC];
             controller.centerController = navigation;
         }
     }];
}

#pragma UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [rearNames count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VRearCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RearCell"];
    
    if (!cell)
        cell = [[VRearCell alloc] init];
    
    [cell updateCellWithTitle:rearNames[indexPath.row]];
    
    return cell;
}

@end
