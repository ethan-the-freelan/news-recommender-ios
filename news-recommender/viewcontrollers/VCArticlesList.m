//
//  VCArticleDetail.m
//  news-recommender
//
//  Created by Ethan Nguyen on 1/11/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCArticlesList.h"
#import "TSMiniWebBrowser.h"
#import "SVPullToRefresh.h"

@interface VCArticlesList ()

@property (nonatomic, assign) NSInteger currentPage;

- (void)getArticles;
- (void)gotoMenu;
- (void)back;

@end

@implementation VCArticlesList

- (id)initWithListing:(NRBase *)listing {
    if (self = [super init]) {
        listingData = listing;
        articlesData = [NSMutableArray array];
        tmpArticleCell = [[VArticleCell alloc] initWithTarget:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([listingData isKindOfClass:[NRSite class]]) {
        [self customTitleWithText:[(NRSite*)listingData title]];
        [self customBarButtonWithImage:@"btn-navbar-back" title:nil target:self action:@selector(back) position:-1];
    } else if ([listingData isKindOfClass:[NRCategory class]]) {
        [self customTitleWithText:[(NRCategory*)listingData name]];
        [self customBarButtonWithImage:@"btn-navbar-back" title:nil target:self action:@selector(back) position:-1];
    } else if (!listingData) {
        [self customTitleWithText:@"HOME"];
        [self customBarButtonWithImage:@"btn-navbar-menu" title:nil target:self action:@selector(gotoMenu) position:-1];
    }
    
    VCArticlesList *blockSelf = self;
    [tblArticles addPullToRefreshWithActionHandler:^{
        blockSelf.currentPage = 0;
        [blockSelf getArticles];
    }];
    
    [tblArticles.pullToRefreshView triggerRefresh];
}

- (void)viewDidUnload {
    tblArticles = nil;
    [super viewDidUnload];
}

- (void)getArticles {
    _currentPage++;
    
    [[NRApi sharedInstance] getArticlesOfListing:listingData atPage:1 handler:^(PNWSRequest *response) {
        [tblArticles.pullToRefreshView stopAnimating];
        ErrorFilter(response);
        
        if (_currentPage == 1)
            [articlesData removeAllObjects];

        [articlesData addObjectsFromArray:(NSArray*)response.response];
        [tblArticles reloadData];
    }];
}

- (void)gotoMenu {
    [self.navigationController.viewDeckController toggleLeftView];
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    [tmpArticleCell updateCellWithData:articlesData[indexPath.row] andCredit:@""];
    return tmpArticleCell.heightToFit;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NRArticle *article = articlesData[indexPath.row];
    [[NRApi sharedInstance] browseArticle:article.Id];
    TSMiniWebBrowser *tsMiniWebBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:article.url]];
    tsMiniWebBrowser.mode = TSMiniWebBrowserModeNavigation;
    [tsMiniWebBrowser customTitleWithText:article.title browserTitle:YES];
    [self.navigationController pushViewController:tsMiniWebBrowser animated:YES];
}

#pragma UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [articlesData count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleCell"];
    
    if (!cell)
        cell = [[VArticleCell alloc] initWithTarget:self];
    
    NRArticle *article = [articlesData objectAtIndex:indexPath.row];

    [cell updateCellWithData:article andCredit:article.site];
    
    return cell;
}

@end
