//
//  VCBaseNavigation.h
//  news-recommender
//
//  Created by Ethan Nguyen on 12/12/12.
//  Copyright (c) 2012 UET. All rights reserved.
//

#import "VCBase.h"

@interface VCBaseNavigation : VCBase

- (void)customBarButtonWithImage:(NSString*)imageName
                           title:(NSString*)title
                          target:(id)target
                          action:(SEL)action
                        position:(NSInteger)position;

- (void)customTitleWithText:(NSString*)title;
- (void)customTitleWithText:(NSString*)title browserTitle:(BOOL)isBrowserTitle;

@end
