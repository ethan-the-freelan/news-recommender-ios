//
//  VCSplashScreen.m
//  news-recommender
//
//  Created by Ethan Nguyen on 1/11/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCSplashScreen.h"
#import "NRAppDelegate.h"
#import "VCArticlesList.h"

@interface VCSplashScreen ()

- (void)goToMainView;

@end

@implementation VCSplashScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [vLoadingIndicator startAnimating];
    
    [[NRApi sharedInstance] getListingsInformation:^(PNWSRequest *response) {
        [self goToMainView];
    }];
}

- (void)viewDidUnload {
    vLoadingIndicator = nil;
    [super viewDidUnload];
}

- (void)goToMainView {
    VCRear *vcRear = [[VCRear alloc] init];
//    UINavigationController *rearNavigation = [[UINavigationController alloc] initWithRootViewController:vcRear];
    VCArticlesList *vcHomeListing = [[VCArticlesList alloc] initWithListing:nil];
    UINavigationController *homeNavigation = [[UINavigationController alloc] initWithRootViewController:vcHomeListing];
    
    IIViewDeckController *viewDeckController = [[IIViewDeckController alloc] initWithCenterViewController:homeNavigation
                                                                                       leftViewController:vcRear];
    viewDeckController.leftLedge = 100;
    viewDeckController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:viewDeckController animated:YES completion:^{
        NRAppDelegate *appDelegate = (NRAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.window.rootViewController = viewDeckController;
    }];
}

@end
