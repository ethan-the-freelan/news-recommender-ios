//
//  VCBase.m
//  news-recommender
//
//  Created by Ethan Nguyen on 12/12/12.
//  Copyright (c) 2012 UET. All rights reserved.
//

#import "VCBase.h"

@interface VCBase ()

@end

@implementation VCBase

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (void)showLoadingHudInView:(UIView *)view withText:(NSString *)text {
    loadingHud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    loadingHud.labelText = text;
    loadingHud.animationType = MBProgressHUDAnimationZoom;
    loadingHud.dimBackground = YES;
}

- (void)hideLoadingHud {
    [loadingHud hide:YES];
}

@end
