//
//  VCSplashScreen.h
//  news-recommender
//
//  Created by Ethan Nguyen on 1/11/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCRear.h"

@interface VCSplashScreen : VCBaseNavigation {
    IBOutlet UIActivityIndicatorView *vLoadingIndicator;
}

@end
