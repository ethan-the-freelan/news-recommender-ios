//
//  VCBase.h
//  news-recommender
//
//  Created by Ethan Nguyen on 12/12/12.
//  Copyright (c) 2012 UET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "IIViewDeckController.h"
#import "NRApi.h"

@interface VCBase : UIViewController {
    MBProgressHUD *loadingHud;
}

- (void)showLoadingHudInView:(UIView*)view withText:(NSString*)text;
- (void)hideLoadingHud;

@end
