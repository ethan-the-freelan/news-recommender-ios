//
//  VCBrowseScreen.h
//  news-recommender
//
//  Created by Ethan Nguyen on 3/24/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCBaseNavigation.h"

@interface VCBrowseScreen : VCBaseNavigation <UITableViewDelegate, UITableViewDataSource> {
    NSArray *listingsData;
    IBOutlet UITableView *tblListings;
}

- (id)initWithListings:(NSArray*)listings andTitle:(NSString*)title;

@end
