//
//  VCBaseNavigation.m
//  news-recommender
//
//  Created by Ethan Nguyen on 12/12/12.
//  Copyright (c) 2012 UET. All rights reserved.
//

#import "VCBaseNavigation.h"

@interface VCBaseNavigation ()

@end

@implementation VCBaseNavigation

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"img-navbar-bg"]
                                                  forBarMetrics:UIBarMetricsDefault];
}

- (void)customBarButtonWithImage:(NSString *)imageName
                           title:(NSString*)title
                          target:(id)target
                          action:(SEL)action
                        position:(NSInteger)position {
    UIImage *image = [UIImage imageNamed:imageName];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    
    if (title)
        [button setTitle:title forState:UIControlStateNormal];
    
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button addTarget:target
               action:action
     forControlEvents:UIControlEventTouchUpInside];
    
    
    UIView *view = [[UIView alloc] initWithFrame:button.frame];
    view.bounds = CGRectOffset(view.bounds, position*7, 0);
    [view addSubview:button];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    
    if (position == 1)
        self.navigationItem.rightBarButtonItem = barButton;
    else
        self.navigationItem.leftBarButtonItem = barButton;
}

- (void)customTitleWithText:(NSString*)title {
    [self customTitleWithText:title browserTitle:NO];
}

- (void)customTitleWithText:(NSString*)title browserTitle:(BOOL)isBrowserTitle {
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.backgroundColor = [UIColor clearColor];
    
    if (isBrowserTitle) {
        lblTitle.font = [UIFont fontWithName:@"CharisSIL" size:16];
        lblTitle.text = title;
    } else {
        lblTitle.font = [UIFont fontWithName:@"CharisSIL-Bold" size:20];
        lblTitle.text = [title uppercaseString];
    }
    
    lblTitle.textColor = [UIColor colorWithRed:241./255 green:226./255 blue:226./255 alpha:1];
    [lblTitle sizeToFit];
    
    if (lblTitle.frame.size.width > 220) {
        CGRect frame = lblTitle.frame;
        frame.size.width = 220;
        lblTitle.frame = frame;
    }
    
    UIView *lblTitleView = [[UIView alloc] initWithFrame:lblTitle.frame];
    [lblTitleView addSubview:lblTitle];
    
    self.navigationItem.titleView = lblTitleView;
}

@end
