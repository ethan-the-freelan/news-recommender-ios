//
//  VRearCell.m
//  news-recommender
//
//  Created by Ethan Nguyen on 4/9/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VRearCell.h"

@implementation VRearCell

- (id)init {
    if (self = [super init]) {
        LoadNibNameWithSameClass();
        lblTitle.font = [UIFont fontWithName:@"CharisSIL-Bold" size:18];
    }
    return self;
}

- (void)updateCellWithTitle:(NSString *)title {
    lblTitle.text = title;
    
    NSString *iconName = [[[title componentsSeparatedByString:@" "] componentsJoinedByString:@"-"] lowercaseString];
    imgIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"img-rear-%@", iconName]];
}

@end
