//
//  VArticleCell.h
//  news-recommender
//
//  Created by Ethan Nguyen on 1/11/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VCBase.h"

@interface VArticleCell : UITableViewCell {
    NRArticle *articleData;
    
    CGFloat heightToFit;
    
    IBOutlet UIImageView *imgCoverImage;
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblCredit;
    IBOutlet UILabel *lblSummary;
}

@property (nonatomic, strong) id<ArticleDelegate> delegate;
@property (nonatomic, readonly) CGFloat heightToFit;

- (id)initWithTarget:(id<ArticleDelegate>)target;
- (void)updateCellWithData:(NRArticle*)article andCredit:(NSString*)credit;
+ (CGFloat)heightToFitWithArticle:(NRArticle*)article;

@end
