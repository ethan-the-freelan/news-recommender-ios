//
//  VArticleCell.m
//  news-recommender
//
//  Created by Ethan Nguyen on 1/11/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VArticleCell.h"
#import "NSDate+HumanizedTime.h"

@implementation VArticleCell

@synthesize heightToFit;

- (id)initWithTarget:(id<ArticleDelegate>)target {
    if (self = [super init]) {
        LoadNibNameWithSameClass();
        _delegate = target;
        lblTitle.font = [UIFont fontWithName:@"CharisSIL-Bold" size:15];
        lblCredit.font = [UIFont fontWithName:@"CharisSIL-Italic" size:10];
        lblSummary.font = [UIFont fontWithName:@"CharisSIL" size:15];
    }
    return self;
}

- (void)updateCellWithData:(NRArticle *)article andCredit:(NSString *)credit {
    articleData = article;
    
    [imgCoverImage setImageWithURL:[NSURL URLWithString:articleData.image]];
    lblTitle.text = articleData.title;
    lblSummary.text = articleData.summary;
    lblCredit.text = [NSString stringWithFormat:@"%@",
                      [articleData.posted_time stringWithHumanizedTimeDifference:NSDateHumanizedSuffixAgo
                                                                  withFullString:YES
                                                               autoPluralization:YES]];
    
    CGRect frame = lblTitle.frame;
    CGSize size = [lblTitle sizeThatFits:CGSizeMake(lblTitle.frame.size.width, MAXFLOAT)];
    frame.size.height = size.height;
    lblTitle.frame = frame;
    
    frame = lblCredit.frame;
    size = [lblCredit sizeThatFits:CGSizeMake(lblCredit.frame.size.width, MAXFLOAT)];
    frame.origin.y = lblTitle.frame.origin.y + lblTitle.frame.size.height + 5;
    frame.size.height = size.height;
    lblCredit.frame = frame;
    
    size = [lblSummary sizeThatFits:CGSizeMake(lblSummary.frame.size.width, MAXFLOAT)];
    frame = lblSummary.frame;
    frame.size.height = size.height;
    lblSummary.frame = frame;
    
    heightToFit = lblSummary.frame.origin.y + lblSummary.frame.size.height + 20;
}

+ (CGFloat)heightToFitWithArticle:(NRArticle *)article {
    CGSize size = [article.title sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:13]
                            constrainedToSize:CGSizeMake(290, MAXFLOAT)];
    return size.height + 120;
}

@end
