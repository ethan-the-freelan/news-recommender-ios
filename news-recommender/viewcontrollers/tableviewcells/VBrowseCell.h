//
//  VBrowseCell.h
//  news-recommender
//
//  Created by Ethan Nguyen on 4/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NRBase.h"

@interface VBrowseCell : UITableViewCell {
    IBOutlet UIImageView *imgLogo;
    IBOutlet UILabel *lblTitle;
    
    NRBase *browseData;
}

- (void)updateCellWithData:(NRBase*)data;

@end
