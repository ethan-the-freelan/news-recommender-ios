//
//  VBrowseCell.m
//  news-recommender
//
//  Created by Ethan Nguyen on 4/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "VBrowseCell.h"
#import "NRApi.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation VBrowseCell

- (id)init {
    if (self = [super init]) {
        LoadNibNameWithSameClass();
        lblTitle.font = [UIFont fontWithName:@"GentiumBasic-Bold" size:18];
    }
    return self;
}

- (void)updateCellWithData:(NRBase *)data {
    browseData = data;
    
    if ([browseData isKindOfClass:[NRSite class]]) {
        lblTitle.text = [(NRSite*)browseData title];
        [imgLogo setImageWithURL:[NSURL URLWithString:[(NRSite*)browseData logo]]];
    } else if ([browseData isKindOfClass:[NRCategory class]])
        lblTitle.text = [(NRCategory*)browseData name];
}

@end
