//
//  VRearCell.h
//  news-recommender
//
//  Created by Ethan Nguyen on 4/9/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VRearCell : UITableViewCell {
    IBOutlet UIImageView *imgIcon;
    IBOutlet UILabel *lblTitle;
}

- (void)updateCellWithTitle:(NSString*)title;

@end
