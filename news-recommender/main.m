//
//  main.m
//  news-recommender
//
//  Created by Ethan Nguyen on 12/12/12.
//  Copyright (c) 2012 UET. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NRAppDelegate class]));
    }
}
