//
//  NRAppDelegate.m
//  news-recommender
//
//  Created by Ethan Nguyen on 12/12/12.
//  Copyright (c) 2012 UET. All rights reserved.
//

#import "NRAppDelegate.h"

@interface NRAppDelegate ()

- (void)preSettings;

@end

@implementation NRAppDelegate

@synthesize window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self preSettings];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    VCSplashScreen *vcSplashScreen = [[VCSplashScreen alloc] init];
    self.window.rootViewController = vcSplashScreen;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

- (void)preSettings {
    [[NRApi sharedInstance] initializeAPI:kApiPath];
}

@end
