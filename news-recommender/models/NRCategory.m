//
//  NRCategory.m
//  news-recommender
//
//  Created by Ethan Nguyen on 3/24/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "NRCategory.h"

@implementation NRCategory

- (NSString*)description {
    return [NSString stringWithFormat:@"NRCategory <Id: %@ - Name: %@>", _Id, _name];
}

@end
