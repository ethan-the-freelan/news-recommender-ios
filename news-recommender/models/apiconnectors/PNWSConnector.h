//
//  PNWSConnector.h
//  houseme
//
//  Created by Lion User on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PNWSRequest.h"
#import "JSONKit.h"
#import "PNRequestBlobItem.h"

@interface PNWSConnector : NSObject

@property (nonatomic,strong) NSString *url;
@property (nonatomic,strong) NSString *sessionToken;

- (id)initWithUrl:(NSString *)url;
- (void)send:(PNWSRequest *)request;
- (PNWSRequest *)defaultRequest;

@end
