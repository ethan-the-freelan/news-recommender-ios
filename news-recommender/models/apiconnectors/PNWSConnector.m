//
//  PNWSConnector.m
//  houseme
//
//  Created by Lion User on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PNWSConnector.h"

//#define DEBUG

@interface PNWSConnector ()

- (void)executeRequest:(PNWSRequest *)request;
- (void)executeTransform:(PNWSRequest *)request;

@end

NSString *const HTTP_METHODS[] = {
    @"GET", @"POST", @"PUT", @"DELETE"
};

@implementation PNWSConnector

@synthesize url;
@synthesize sessionToken;

- (id)initWithUrl:(NSString *)_url {
    if (self = [super init])
        self.url = _url;
    
    return self;
}

- (void)send:(PNWSRequest *)request {
    ShowNetworkActivityIndicator();
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self executeRequest:request];
    });
}

- (void)executeRequest:(PNWSRequest *)request {
    NSMutableString *urlString = [NSMutableString string];
    
    // If request path is full format URI
    if ([request.path rangeOfString:@"http"].location == NSNotFound)
        urlString = [NSMutableString stringWithFormat:@"%@/%@",
                     self.url, request.path];
    else
        urlString = [request.path mutableCopy];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    
    urlRequest = [self generateRequest:urlString
                                  data:request.data
                                method:request.method];
    
    NSHTTPURLResponse *response;
    NSError *error;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:urlRequest
                                                 returningResponse:&response
                                                             error:&error];
    
    if (responseData == nil) {
        NSError *error = [NSError errorWithDomain:@"NETWORK_ERROR"
                                             code:response.statusCode
                                         userInfo:nil];
        request.response = nil;
        request.error = error;
        [self executeTransform:request];
        return;
    }
    
    if ([response statusCode] != 200) {
        NSDictionary *responseDict =
        [NSDictionary dictionaryWithObjectsAndKeys:
         [[NSString alloc] initWithData:responseData
                               encoding:NSUTF8StringEncoding],
         @"response",
         nil];
        
        NSError *error = [NSError errorWithDomain:[NSString stringWithFormat:
                                                   @"STATUS_CODE_%d",
                                                   [response statusCode]]
                                             code:response.statusCode
                                         userInfo:responseDict];
        request.response = nil;
        request.error = error;
        [self executeTransform:request];
        return;
    }
    
    NSString *responseString = [[NSString alloc] initWithData:responseData
                                                     encoding:NSUTF8StringEncoding];
    
#ifdef DEBUG
    DLog(@"response %@", responseString);
#endif
    
    NSObject *json = [responseString objectFromJSONString];
    
    if (!json) {
        NSError *error = [NSError errorWithDomain:@"NON_JSON_RESPONSE"
                                             code:-1
                                         userInfo:(NSDictionary*)json];
        request.response = nil;
        request.error = error;
        [self executeTransform:request];
        return;
    }
    
    request.response = json;
    request.error = nil;
    [self executeTransform:request];
}

- (void)executeTransform:(PNWSRequest *)request {
    dispatch_sync(dispatch_get_main_queue(), ^{
        HideNetworkActivityIndicator();
        request.transformer(request);
    });
}

- (NSMutableURLRequest *)generateRequest:(NSString *)urlString
                                    data:(NSDictionary *)data
                                  method:(PNWSRequestMethod)method {
    // If request is obsolete
    if ([[data objectForKey:kParamRequestType] isEqualToString:kParamValueObsolete]) {
        NSMutableDictionary *mutableData = [data mutableCopy];
        [mutableData removeObjectForKey:kParamRequestType];
        return [self generateMultiFormRequest:urlString
                                         data:mutableData
                                       method:method];
    }
    
    if (method == PNWSRequestMethodGet)
        return [self generateGetRequest:urlString data:data];
    
    return [self generateJSONRequest:urlString data:data method:method];
}

- (NSMutableURLRequest *)generateJSONRequest:(NSString *)urlString
                                        data:(NSDictionary *)data
                                      method:(PNWSRequestMethod)method {
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    
    NSData *postData = [data JSONData];
    
    [urlRequest setURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:[NSString stringWithFormat:@"%d", [postData length]]
      forHTTPHeaderField:@"Content-Length"];
    [urlRequest setValue:@"json" forHTTPHeaderField:@"Data-Type"];
    [urlRequest setHTTPBody:postData];
    [urlRequest setHTTPMethod:HTTP_METHODS[method]];
    
#ifdef DEBUG
    DLog(@"%@", urlRequest);
#endif
    
    return urlRequest;
}

- (NSMutableURLRequest *)generateHttpRequest:(NSString *)urlString
                                        data:(NSDictionary *)data
                                      method:(PNWSRequestMethod)method {
    for (NSString *key in data)
        if ([[data objectForKey:key] isKindOfClass:[PNRequestBlobItem class]])
            return [self generateMultiFormRequest:urlString
                                             data:data
                                           method:method];
    
    return [self generateNormalRequest:urlString
                                  data:data
                                method:method];
}

- (NSMutableURLRequest *)generateNormalRequest:(NSString*)urlString
                                          data:(NSDictionary*)data
                                        method:(PNWSRequestMethod)method {
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    
    NSString *queryString = [self generateQueryString:data];
    
#ifdef DEBUG
    DLog(@"request url %@", urlString);
    DLog(@"request body %@", queryString);
#endif
    
    [urlRequest setURL:[NSURL URLWithString:urlString]];
    [urlRequest setHTTPMethod:HTTP_METHODS[method]];
    [urlRequest setHTTPBody:[queryString dataUsingEncoding:NSUTF8StringEncoding]];
    
    return urlRequest;
}

- (NSMutableURLRequest *)generateMultiFormRequest:(NSString *)urlString
                                             data:(NSDictionary *)data
                                           method:(PNWSRequestMethod)method {
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    
    NSString *newLine = @"\r\n";
    NSString *boundary =
    @"---------------------------Boundary Line---------------------------";
    
    NSData *boundaryData = [[NSString stringWithFormat:@"%@--%@%@",
                             newLine, boundary, newLine]
                            dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableData *postData = [NSMutableData data];
    
    for (NSString *key in data) {
        id object = [data objectForKey:key];
        if ([object isKindOfClass:[NSArray class]]) {
            for (NSString *value in (NSArray*)object) {
                [postData appendData:boundaryData];
                [postData appendData:
                 [[NSString stringWithFormat:
                   @"Content-Disposition: form-data; name=\"%@\"%@%@", 
                   key, newLine, newLine]
                  dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:
                 [value dataUsingEncoding:NSUTF8StringEncoding]];                
            }
        } else if ([object isKindOfClass:[PNRequestBlobItem class]]) {
            PNRequestBlobItem *blobItem = (PNRequestBlobItem *)object;
            [postData appendData:boundaryData];
            [postData appendData:
             [[NSString stringWithFormat:
               @"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"%@",
               key, blobItem.fileName, newLine]
              dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:
             [[NSString stringWithFormat:@"Content-Type: %@%@%@",
               blobItem.contentType, newLine, newLine]
              dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:blobItem.data];
        } else {
            [postData appendData:boundaryData];
            [postData appendData:
             [[NSString stringWithFormat:
               @"Content-Disposition: form-data; name=\"%@\"%@%@",
               key, newLine, newLine]
              dataUsingEncoding:NSUTF8StringEncoding]];
            
            [postData appendData:[object dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
#ifdef DEBUG
    DLog(@"request url: %@", urlString);
    DLog(@"request body (null if there is blob): %@",
         [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding]);
#endif
    
    [postData appendData:[[NSString stringWithFormat:@"%@--%@--%@",
                           newLine, boundary, newLine]
                          dataUsingEncoding:NSUTF8StringEncoding]];
    
    [urlRequest setURL:[NSURL URLWithString:urlString]];
    [urlRequest setHTTPMethod:HTTP_METHODS[method]];
    NSString *contentType =
    [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
	[urlRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [urlRequest setHTTPBody:postData];
    
    return urlRequest;
}

- (NSString *)generateQueryString:(NSDictionary *)data {
    NSMutableString *queryString = [NSMutableString string];
    for (NSString *key in data) {
        if ([[data objectForKey:key] isKindOfClass:[NSArray class]]) {
            for (NSString *value in [data objectForKey:key]) {
                [queryString appendFormat:@"%@=%@&", key,
                 [value stringByAddingPercentEscapesUsingEncoding:
                  NSUTF8StringEncoding]];
            }
        } else {
            [queryString appendFormat:@"%@=%@&", key,
             [[data objectForKey:key]
              stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    return queryString;
}

- (NSMutableURLRequest *)generateGetRequest:(NSString *)_urlString
                                       data:(NSDictionary *)data {
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSMutableString *urlString = [NSMutableString stringWithString:_urlString];
    NSString *queryString = [self generateQueryString:data];
    
    if ([queryString length] > 0)
        [urlString appendFormat:@"?%@", queryString];
    
#ifdef DEBUG
    DLog(@"request url %@", urlString);
    DLog(@"request body %@", queryString);
#endif
    
    [urlRequest setURL:[NSURL URLWithString:urlString]];
    [urlRequest setHTTPMethod:@"GET"];
    
    return urlRequest;
}

- (PNWSRequest *)defaultRequest {
    return [[PNWSRequest alloc] init];
}

#ifdef DEBUG
#undef DEBUG
#endif

@end
