//
//  PNWSRequest.m
//  houseme
//
//  Created by Lion User on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PNWSRequest.h"

@implementation PNWSRequest

@synthesize data, response, path, method, error;
@synthesize transformer;

- (id)init {
    if (self = [super init])
        self.method = PNWSRequestMethodGet;
    
    return self;
}

@end
