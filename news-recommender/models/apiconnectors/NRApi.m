//
//  NRApi.m
//  news-recommender
//
//  Created by Ethan Nguyen on 12/12/12.
//  Copyright (c) 2012 UET. All rights reserved.
//

#import "NRApi.h"

@implementation NRApi

+ (id)sharedInstance {
    static NRApi *sharedApi = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedApi = [[self alloc] init];
    });
    return sharedApi;
}

- (void)initializeAPI:(NSString *)url {
    apiConnector = [[PNWSConnector alloc] initWithUrl:url];
}

- (void)sendRequest:(NSDictionary *)data
               path:(NSString *)path
             method:(PNWSRequestMethod)method
        transformer:(ResponseTransformBlock)transformer {
    NSMutableDictionary *newData = [data mutableCopy];
    if (apiConnector.sessionToken)
        [newData setValue:apiConnector.sessionToken forKey:@"token"];
    
    PNWSRequest *request = [apiConnector defaultRequest];
    request.path = path;
    request.data = newData;
    request.method = method;
    request.transformer = transformer;
    
    [apiConnector send:request];
}

- (void)getListingsInformation:(ResponseHandlerBlock)handler {
    NSDictionary *request = @{@"udid" : [[UIDevice currentDevice] uniqueDeviceIdentifier]};
    
    [self sendRequest:request
                 path:@"sites/all_info"
               method:PNWSRequestMethodGet
          transformer:^(PNWSRequest *response) {
              ResponseFilter(response);
              
              for (NSString *listingName in @[@"sites", @"categories"]) {
                  NSArray *listingsArr = [(NSDictionary*)response.response objectForKey:listingName];
                  
                  NSMutableArray *listings = [NSMutableArray array];
                  
                  for (NSDictionary *dict in listingsArr) {
                      NRBase *listing;
                      if ([listingName isEqualToString:@"sites"])
                          listing = [[NRSite alloc] initWithDict:dict];
                      else
                          listing = [[NRCategory alloc] initWithDict:dict];
                      
                      [listings addObject:listing];
                  }
                  
                  if ([listingName isEqualToString:@"sites"])
                      [[NRVertical currentVertical] setSites:listings];
                  else
                      [[NRVertical currentVertical] setCategories:listings];
              }
              
              handler(nil);
          }];
}

- (void)getArticlesOfListing:(NRBase *)listing atPage:(NSInteger)page handler:(ResponseHandlerBlock)handler {
    NSMutableDictionary *request = [@{
                                    @"udid": [[UIDevice currentDevice] uniqueDeviceIdentifier],
                                    @"page" : [NSString stringWithFormat:@"%d", page]
                                    } mutableCopy];
    
    NSString *path = @"";
    
    if ([listing isKindOfClass:[NRSite class]]) {
        [request setObject:listing.Id forKey:@"site_id"];
        path = @"articles/by_site";
    } else if ([listing isKindOfClass:[NRCategory class]]) {
        [request setObject:listing.Id forKey:@"category_id"];
        path = @"articles/by_category";
    } else
        path = @"users/recommended_articles";
    
    [self sendRequest:request
                 path:path
               method:PNWSRequestMethodGet
          transformer:^(PNWSRequest *response) {
              ResponseFilter(response);
              
              NSArray *articlesArr = [(NSDictionary*)response.response objectForKey:@"results"];
              
              NSMutableArray *articles = [NSMutableArray array];
              
              for (NSDictionary *dict in articlesArr) {
                  NRArticle *article = [[NRArticle alloc] initWithDict:dict];
                  [articles addObject:article];
              }
              
              response.response = articles;
              handler(response);
          }];
}

- (void)browseArticle:(NSString *)articleId {
    NSDictionary *request = @{
                              @"udid" : [[UIDevice currentDevice] uniqueDeviceIdentifier],
                              @"article_id" : articleId
                              };
    
    [self sendRequest:request
                 path:@"users/browse_article"
               method:PNWSRequestMethodPost
          transformer:^(PNWSRequest *response) {
          }];
}

@end
