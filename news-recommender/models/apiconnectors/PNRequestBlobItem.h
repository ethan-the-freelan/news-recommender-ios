//
//  PNRequestFileItem.h
//  houseme
//
//  Created by Lion User on 6/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNRequestBlobItem : NSObject

@property (nonatomic,strong) NSData *data;
@property (nonatomic,strong) NSString *contentType;
@property (nonatomic,strong) NSString *fileName;

@end
