//
//  PNWSRequest.h
//  houseme
//
//  Created by Lion User on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@class PNWSRequest;

typedef void (^ResponseTransformBlock)(PNWSRequest *response);

typedef enum {
    PNWSRequestMethodGet = 0,
    PNWSRequestMethodPost,
    PNWSRequestMethodPut,
    PNWSRequestMethodDelete
} PNWSRequestMethod;

@interface PNWSRequest : NSObject

@property (nonatomic,strong) NSDictionary *data;
@property (nonatomic,strong) NSObject *response;
@property (nonatomic,strong) NSString *path;
@property (nonatomic) PNWSRequestMethod method;
@property (nonatomic,strong) NSError *error;
@property (nonatomic,strong) ResponseTransformBlock transformer;

@end
