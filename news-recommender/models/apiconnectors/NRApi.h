//
//  NRApi.h
//  news-recommender
//
//  Created by Ethan Nguyen on 12/12/12.
//  Copyright (c) 2012 UET. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIDevice+IdentifierAddition.h"
#import "CommonDelegates.h"
#import "PNWSConnector.h"
#import "NRVertical.h"
#import "NRSite.h"
#import "NRCategory.h"
#import "NRArticle.h"

#define ResponseFilter(response) \
if (response.error) { \
    if (!handler) \
        return; \
    handler(response); \
    return; \
} \

#define ErrorFilter(response) \
if (!response) { \
    DLog(@"Empty handler"); \
    return; \
} else if (response.error) { \
    DLog(@"%@", response.error); \
    return; \
}

typedef void (^ResponseHandlerBlock)(PNWSRequest *response);

@interface NRApi : NSObject {
    PNWSConnector *apiConnector;
}

+ (id)sharedInstance;
- (void)initializeAPI:(NSString *)url;

- (void)getListingsInformation:(ResponseHandlerBlock)handler;
- (void)getArticlesOfListing:(NRBase*)listing atPage:(NSInteger)page handler:(ResponseHandlerBlock)handler;
- (void)browseArticle:(NSString*)articleId;

@end
