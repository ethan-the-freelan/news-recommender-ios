//
//  NRVertical.m
//  news-recommender
//
//  Created by Ethan Nguyen on 1/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "NRVertical.h"

@implementation NRVertical

@synthesize sitesData = _sitesData;

+ (id)currentVertical {
    static NRVertical *vertical = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        vertical = [[self alloc] init];
    });
    return vertical;
}

- (void)setSites:(NSArray *)aSitesData {
    _sitesData = [aSitesData mutableCopy];
}

- (void)setCategories:(NSArray *)aCategoriesData {
    _categoriesData = [aCategoriesData mutableCopy];
}

@end
