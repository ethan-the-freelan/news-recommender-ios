//
//  NRCategory.h
//  news-recommender
//
//  Created by Ethan Nguyen on 3/24/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "NRBase.h"

@interface NRCategory : NRBase

@property (nonatomic, strong) NSString *Id;
@property (nonatomic, strong) NSString *name;

@end
