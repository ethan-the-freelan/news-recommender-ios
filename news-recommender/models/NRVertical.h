//
//  NRVertical.h
//  news-recommender
//
//  Created by Ethan Nguyen on 1/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NRVertical : NSObject {
    NSMutableArray *sitesData;
}

@property (nonatomic, strong, setter = setSites:, getter = sites) NSArray *sitesData;
@property (nonatomic, strong, setter = setCategories:, getter = categories) NSArray *categoriesData;

+ (id)currentVertical;

@end
