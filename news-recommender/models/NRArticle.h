//
//  NRArticle.h
//  news-recommender
//
//  Created by Ethan Nguyen on 1/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "NRBase.h"

@interface NRArticle : NRBase

@property (nonatomic, strong) NSString *Id;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSDate *posted_time;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSString *site;
@property (nonatomic, strong) NSString *category;

@end
