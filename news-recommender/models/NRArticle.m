//
//  NRArticle.m
//  news-recommender
//
//  Created by Ethan Nguyen on 1/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "NRArticle.h"
#import "NSDate+RFC822Parser.h"

@implementation NRArticle

- (id)initWithDict:(NSDictionary *)dict {
    if (self = [super initWithDict:dict]) {
        _posted_time = [NSDate rfc822DateFromString:dict[@"posted_time"]];
    }
    
    return self;
}

- (NSString*)description {
    return [NSString stringWithFormat:@"NRArticle <Url: %@ - Title: %@>", _url, _title];
}

@end
