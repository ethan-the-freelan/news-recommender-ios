//
//  NRBase.h
//  news-recommender
//
//  Created by Ethan Nguyen on 1/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NRBase : NSObject

- (id)initWithDict:(NSDictionary *)dict;
- (NSString*)Id;

@end
