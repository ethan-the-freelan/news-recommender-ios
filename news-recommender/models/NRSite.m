//
//  NRSite.m
//  news-recommender
//
//  Created by Ethan Nguyen on 1/10/13.
//  Copyright (c) 2013 UET. All rights reserved.
//

#import "NRSite.h"

@implementation NRSite

- (NSString*)description {
    return [NSString stringWithFormat:@"NRSite <Id: %@ - Title: %@>", _Id, _title];
}

@end
